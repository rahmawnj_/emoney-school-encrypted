<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <form action="<?= base_url('classes/student_update') ?>" method="post">

                    <div class=" row removable">
                        <div class="col-lg-12">
                            <div class="card flex-fill">
                                <div class="card-header d-flex justify-content-between">
                                    <h5 class="card-title mb-0"><?= $title ?> - <?= $class['kelas'] ?></h5>
                                </div>
                                <div class="card-body">

                                    <div class="input-group mb-3">
                                        <select name="class" class="form-select flex-grow-1">
                                            <option selected disabled>-- Pilih Kelas --</option>
                                            <?php foreach ($classes as $kelas) : ?>
                                                <option <?= ($class['kelas'] == $kelas['kelas'] ? "selected" : "") ?> value="<?= $kelas['id_class'] ?>"><?= $kelas['kelas'] ?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <input type="hidden" name="id_class" value="<?= $class['id_class'] ?>">

                                        <button class="btn btn-secondary" type="submit">Go!</button>
                                    </div>


                                    <div class="table-responsive">
                                        <table class="table table-hover my-0 ">
                                            <thead>
                                                <tr>
                                                    <th><input type="checkbox" onchange="checkAll(this)" name="chk[]"></th>
                                                    <th>#</th>
                                                    <th>NIS</th>
                                                    <th>Nama</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $no = 0;
                                                foreach ($students as $student) : ?>
                                                    <tr>
                                                        <td><input type="checkbox" name="chkbox[]" value="<?= $student['id_student'] ?>"></td>
                                                        <td><?= ++$no; ?></td>
                                                        <td><?= $student['nis'] ?></td>
                                                        <td><?= $student['nama'] ?></td>

                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>