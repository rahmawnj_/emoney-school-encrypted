<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-title mb-0"><?= $title ?></h5>
                            </div>
                            <div class="card-body">
                                <?= $this->session->flashdata('message'); ?>
                                <form action="<?= base_url('categories/update') ?>" method="post">
                                    <input type="hidden" class="form-control" name="id_category" value="<?= $category['id_category'] ?>">

                                    <div class="form-group">
                                        <label for="kategori">Kategori</label>
                                        <input type="text" autocomplete="off" class="form-control" name="kategori" value="<?= $category['kategori'] ?>" id="kategori" aria-describedby="kategori" placeholder="Masukan Kategori">
                                        <span class="text-danger">
                                            <?= form_error('kategori') ?>
                                        </span>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" value="upload" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>