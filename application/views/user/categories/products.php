<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="col-lg-12">
                        <div class="card flex-fill">
                            <div class="card-header d-flex justify-content-between">
                                <h5 class="card-title mb-0"><?= $title ?> - <?= $category['kategori'] ?></h5>
                            </div>

                            <div class="card-body">
                                <div class="table-responsive">
                                    <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>
                                    <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>

                                    <table id="table" class="table table-hover my-0 ">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama Produk</th>
                                                <th>Stok</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 0;
                                            foreach ($products as $product) : ?>
                                                <tr>
                                                    <td><?= ++$no; ?></td>
                                                    <td><?= $product['product_nama'] ?></td>
                                                    <td><?= $product['product_stok'] ?></td>

                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>

<?php $this->load->view('layout/foot') ?>