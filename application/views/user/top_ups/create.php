<?php $this->load->view('layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('layout/header') ?>

        <main class="content">
        <div class="container-fluid p-0">
            <div class="row removable">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0"><?= $title ?></h5>
                        </div>
                        <div class="card-body">
                            <?= $this->session->flashdata('message'); ?>

                            <form action="<?= base_url('top_ups/store') ?>" method="post">

                                <div class="img-preview d-flex">
                                </div>
                                <div class="container-fluid">
                                    <img src="<?= base_url('assets/img/default_user.jfif') ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:200px" alt="">
                                    <div class="row mt-2 mb-2">
                                        <div class="col-md-2" id="initNama"></div>
                                        <div class="col" id="nama"></div>
                                    </div>
                                    <div class="row mt-2 mb-2">
                                        <div class="col-md-2" id="iniNIS"></div>
                                        <div class="col" id="nis"></div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="rfid_nis">RFID / NIS</label> <br>
                                    <label id="error_rfid_nis" class="text-danger" ></label>
                                    <input type="text" value="<?= set_value('rfid_nis') ?>" autocomplete="off" class="form-control" name="rfid_nis" onkeyup="rfidSearch(this.value)" id="rfid_nis" aria-describedby="rfid_nis" placeholder="Masukan RFID" required>
                                </div>

                                <div class="form-group">
                                    <label for="nominal">Nominal : <b id="rupFormat">Rp. 0</b> </label>

                                    <input type="number" value="<?= set_value('nominal') ?>" autocomplete="off" class="form-control" name="nominal" onkeyup="rupiahFormat()" id="nominal" aria-describedby="nominal" placeholder="Masukan Nominal" required>
                                    <span class="text-danger">
                                        <?= form_error('nominal') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <label for="password">Password Konfirmasi</label>
                                    <input type="password" class="form-control" required autocomplete="off" name="password" id="password" aria-describedby="password" placeholder="Masukan Password">
                                    <span class="text-danger">
                                        <?= form_error('password') ?>
                                    </span>
                                </div>

                                <div class="form-group">
                                    <button type="submit" id="button" class="btn btn-primary">Submit</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

        <?php $this->load->view('layout/footer') ?>
    </div>
</div>
<script>
        function rfidSearch(value) {
            $.post('<?= base_url('data/search_rfid_nis/') ?>', {
                rfid_nis: value
            }, function(data, status) {

                var data = JSON.parse(data)
                if (data.error) {
                    document.getElementById("error_rfid_nis").innerHTML = data.error;
                    document.getElementById("nama").innerHTML = '';
                    document.getElementById("nis").innerHTML = '';
                    document.getElementById("initNama").innerHTML = ''
                    document.getElementById("iniNIS").innerHTML = ''
                    document.getElementById("gmbr").src = '<?= base_url('assets/img/default_user.jfif') ?>'
                    document.getElementById("button").disabled = true;
                } else {
                    document.getElementById("error_rfid_nis").innerHTML = ''
                    document.getElementById("initNama").innerHTML = '<strong>Nama</strong>'
                    document.getElementById("iniNIS").innerHTML = '<strong>NIS</strong>'
                    document.getElementById("nama").innerHTML = data[0].nama
                    document.getElementById("nis").innerHTML =  data[0].nis
                    document.getElementById("gmbr").src = '<?= base_url('assets/img/uploads/') ?>' + data[0].foto,
                    document.getElementById("button").disabled = false;
                }
            })
        }

        function rupiahFormat() { //pembuatan fungsi pemanggila
            var nominal = document.getElementById("nominal").value;
            var nilai = nominal.length; //menghitung banyaknya digit
            var angkatersisa = nilai % 3;
            var angkasisa = "";

            switch (angkatersisa) { //opsi nilai angkatersisa 
                case 0:
                    angkasisa = 3;
                    break;
                default:
                    angkasisa = angkatersisa;
                    break;
            }
            var formatne = nominal.substr(angkasisa); //pengambilan data yang dimulai dari jumlah sisa dari hasil pembagian 3 atau angkasisa sampai digit terakhir
            var panjangformat = formatne.length / 3;
            var pemisah = ""; //pendeklarasian variabel untuk angka yang akan diambil per tiga digit dan ditandai dengan koma atau titik
            for (i = 0; i < panjangformat; i++) {
                pemisah = "." + formatne.substr(-3) + pemisah;
                var sisanilai = formatne.length - 3;
                formatne = formatne.substr(0, sisanilai);
            }
            var hasil = nominal.substr(0, angkasisa); //mengambil digit terdapan dari 0 sampai sisa dari hasil pembagian 3
            if (nominal.value != "") {
                document.getElementById("rupFormat").innerHTML = "Rp. " + hasil + pemisah;
            } else {
                document.getElementById("rupFormat").innerHTML = "";
            }
        }
    </script>

<?php $this->load->view('layout/foot') ?>

