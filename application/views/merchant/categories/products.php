<?php $this->load->view('merchant_layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('merchant_layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('merchant_layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="col-lg-12">
                        <div class="card flex-fill">
                            <div class="card-header d-flex justify-content-between">
                                <h5 class="card-title mb-0"><?= $title ?> - <?= $category['kategori'] ?></h5>
                            </div>

                            <div class="card-body">
                                <div class="table-responsive">
                                    <div class="flash-data" data-flashdata="<?= $this->session->flashdata('message') ?>"></div>
                                    <table class="table table-hover my-0 ">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nama Produk</th>
                                                <th>Stok</th>
                                                <th>Harga</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 0;
                                            foreach ($products as $product) : ?>
                                                <tr>
                                                    <td><?= ++$no; ?></td>
                                                    <td><?= $product['nama'] ?></td>
                                                    <td><?= $product['stok'] ?></td>
                                                    <td><?= $product['harga_jual'] ?></td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php $this->load->view('merchant_layout/footer') ?>
    </div>
</div>

<?php $this->load->view('merchant_layout/foot') ?>