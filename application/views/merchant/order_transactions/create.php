<?php $this->load->view('merchant_layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('merchant_layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('merchant_layout/header') ?>

        <main class="content">
            <div class="container-fluid p-0">
                <div class="row removable">
                    <div class="card">
                        <div class="m-5">
                            <div class="row g-5">
                                <div class="col-md-5 col-lg-4 order-md-last">
                                    <h4 class="d-flex justify-content-between align-items-center mb-3">
                                        <span class="text-primary"><i class="fa fa-shopping-basket"></i> Keranjang</span>
                                        <span class="badge bg-primary rounded-pill"><?= $jumlah ?></span>
                                    </h4>
                                    <ul class="list-group mb-3">
                                        <?php foreach ($carts as $cart) : ?>
                                            <li class="list-group-item d-flex justify-content-between lh-sm">
                                                <div>
                                                    <h6 class="my-0"><?= $cart['nama'] ?> x <?= $cart['jumlah'] ?></h6>
                                                    <small class="text-muted"><?= "Rp " . number_format($cart['harga_jual'], 0, ',', '.') ?> </small>
                                                </div>
                                                <span class="text-muted"><?= "Rp " . number_format($cart['harga_jual'] * $cart['jumlah'], 0, ',', '.') ?></span>
                                            </li>
                                        <?php endforeach ?>
                                        <li class="list-group-item d-flex justify-content-between">
                                            <span>Total </span>
                                            <strong><?= "Rp " . number_format($total_harga, 0, ',', '.') ?></strong>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-7 col-lg-8">
                                    <h4 class="mb-3">Checkout</h4>
                                    <div class="container-fluid">
                                        <img src="<?= base_url('assets/img/default_user.jfif') ?>" id="gmbr" class="img-fluid img-thumbnail mx-auto d-block text-center" style="height:200px" alt="">
                                        <div class="row mt-2 mb-2">
                                            <div class="col-md-2" id="initNama"></div>
                                            <div class="col" id="nama"></div>
                                        </div>
                                        <div class="row mt-2 mb-2">
                                            <div class="col-md-2" id="iniNIS"></div>
                                            <div class="col" id="nis"></div>
                                        </div>
                                    </div>
                                    <form class="needs-validation" method="post" action="<?= base_url('order_transactions/store') ?>">
                                        <div class="flash-data-error" data-flashdataerror="<?= $this->session->flashdata('error') ?>"></div>

                                        <?= $this->session->flashdata('message'); ?>

                                        <div class="form-group">
                                            <select name="payment_method" class="form-control mb-3 payment_method" id="payment_method" required>
                                                <option selected disabled>-- Pilih Metode Pembayaran --</option>
                                                <option value="cash">Cash</option>
                                                <option value="emoney">E - Money</option>
                                            </select>
                                            <span class="text-danger">
                                                <?= form_error('payment_method') ?>
                                            </span>
                                        </div>

                                        <div class="collapse" id="collapse-emoney">
                                            <p id="error_rfid"></p>

                                            <div class="form-group">
                                                <input type="hidden" name="total_harga" value="<?= $total_harga ?>">
                                                <div class="form-floating mb-3">
                                                    <input type="rfid" class="form-control" autocomplete="rfid" name="rfid" onkeyup="rfidSearch(this.value)" value="<?= set_value('rfid') ?>" id="rfid" placeholder="Masukan RFID">
                                                    <label for="rfid">Masukan RFID</label>
                                                </div>
                                                <span class="text-danger">
                                                    <?= form_error('rfid') ?>
                                                </span>
                                            </div>

                                            <div class="form-group">
                                                <div class="form-floating mb-3">
                                                    <input type="password" autocomplete="off" class="form-control" name="pin" value="<?= set_value('pin') ?>" id="pin" placeholder="Masukan Pin">
                                                    <label for="pin">Masukan Pin</label>
                                                </div>
                                                <span class="text-danger">
                                                    <?= form_error('pin') ?>
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" id="button-emoney" class="btn btn-primary">Submit</button>
                                            </div>
                                        </div>

                                        <div class="collapse" id="collapse-cash">
                                            <p id="warning_nama"></p>

                                            <div class="form-group">
                                                <input type="hidden" name="total_harga" value="<?= $total_harga ?>">
                                                <div class="form-floating mb-3">
                                                    <input type="text" autocomplete="off" class="form-control" name="nama" onkeyup="namaSearch(this.value)" value="<?= set_value('nama') ?>" id="nama" placeholder="Masukan Nama">
                                                    <label for="nama">Masukan Nama</label>
                                                </div>
                                                <span class="text-danger">
                                                    <?= form_error('nama') ?>
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <button type="submit" id="button-cash" class="btn btn-primary">Submit</button>
                                            </div>

                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php $this->load->view('merchant_layout/footer') ?>
    </div>
</div>
<script>
    function namaSearch(value) {
        if (value != '') {
            $.post('<?= base_url('data/search_nama/') ?>', {
                nama_nis: value
            }, function(data, status) {
                var data = JSON.parse(data)
                if (data.warning) {
                    console.log(data.warning);
                    document.getElementById("warning_nama").innerHTML = data.warning;
                    document.getElementById("nama").innerHTML = '';
                    document.getElementById("nis").innerHTML = '';
                    document.getElementById("initNama").innerHTML = ''
                    document.getElementById("iniNIS").innerHTML = ''
                    document.getElementById("gmbr").src = '<?= base_url('assets/img/default_user.jfif') ?>'
                } else {
                    document.getElementById("warning_nama").innerHTML = ''
                    document.getElementById("initNama").innerHTML = '<strong>Nama</strong>'
                    document.getElementById("iniNIS").innerHTML = '<strong>NIS</strong>'
                    document.getElementById("nama").innerHTML = data[0].nama
                    document.getElementById("nis").innerHTML = data[0].nis
                    document.getElementById("gmbr").src = '<?= base_url('assets/img/uploads/') ?>' + data[0].foto
                }
            })
        }

    }

    function rfidSearch(value) {
        $.post('<?= base_url('data/search_rfid/') ?>', {
            rfid: value
        }, function(data, status) {
            var data = JSON.parse(data)
            if (data.error) {
                document.getElementById("error_rfid").innerHTML = data.error;
                document.getElementById("nama").innerHTML = '';
                document.getElementById("nis").innerHTML = '';
                document.getElementById("initNama").innerHTML = ''
                document.getElementById("iniNIS").innerHTML = ''
                document.getElementById("gmbr").src = '<?= base_url('assets/img/default_user.jfif') ?>'
                document.getElementById("button-emoney").disabled = true;

            } else {
                document.getElementById("error_rfid").innerHTML = ''
                document.getElementById("initNama").innerHTML = '<strong>Nama</strong>'
                document.getElementById("iniNIS").innerHTML = '<strong>NIS</strong>'
                document.getElementById("nama").innerHTML = data[0].nama
                document.getElementById("nis").innerHTML = data[0].nis
                document.getElementById("gmbr").src = '<?= base_url('assets/img/uploads/') ?>' + data[0].foto
                document.getElementById("button-emoney").disabled = false;
            }

        })
    }
</script>
<?php $this->load->view('merchant_layout/foot') ?>