<?php $this->load->view('merchant_layout/head') ?>

<div class="wrapper">
    <?php $this->load->view('merchant_layout/sidebar') ?>
    <div class="main">
        <?php $this->load->view('merchant_layout/header') ?>

        <main class="content">
        <div class="container-fluid p-0">
            <div class="row removable">
                <div class="col-lg-12">
                    <div class="card flex-fill">
                        <div class="card-header d-flex justify-content-between">
                            <h5 class="card-title mb-0"><?= $title ?></h5>
                            <a href="<?= base_url('merchant/products/create') ?>" class="btn btn-primary float-right fas fa-plus"></a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                            <div class="flash-data-success" data-flashdatasuccess="<?= $this->session->flashdata('success') ?>"></div>

                                <table class="table table-hover my-0" id="table">

                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Nama Barang</th>
                                            <th>Jumlah</th>
                                            <th>Modal</th>
                                            <th>Total</th>
                                            <th>Keuntungan</th>
                                            <th>Tanggal</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $no = 0;
                                        foreach ($transactions as $product) : ?>
                                            <tr>
                                                <td><?= ++$no; ?></td>
                                                <td><?= $product['product_nama'] ?></td>
                                                <td><?= $product['product_stok'] ?></td>

                                                <td>
                                                    <a class="fa fa-edit btn bg-warning text-white" href="<?= base_url('merchant/products/edit/' . $product['id_product']) ?>"></a>
                                                    <a class="fas fa-eye btn bg-info text-white" href="<?= base_url('merchant/products/' . $product['id_product']) ?>"></a>
                                                    <a id="delete-button" class="fas fa-trash btn bg-danger text-white" href="<?= base_url('products/merchant_delete/' . $product['id_product']) ?>"></a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </main>

        <?php $this->load->view('merchant_layout/footer') ?>
    </div>
</div>

<?php $this->load->view('merchant_layout/foot') ?>

