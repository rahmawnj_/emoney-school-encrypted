<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            margin: 0;
            padding: 0;
        }

        p {
            margin: 0;
            padding: 0;
        }

        .container {
            width: 80%;
            margin-right: auto;
            margin-left: auto;
        }

        .logo {
            width: 50%;
        }

        .row {
            display: flex;
            flex-wrap: wrap;
        }

        .col-6 {
            width: 50%;
            flex: 0 0 auto;
        }

        .text-white {
            color: #fff;
        }

        .heading {
            font-size: 20px;
            margin-bottom: 08px;
        }

        .sub-heading {
            color: #262626;
            margin-bottom: 05px;
        }

        table {
            background-color: #fff;
            width: 100%;
            border-collapse: collapse;
        }

        table thead tr {
            border: 1px solid #111;
            background-color: #f2f2f2;
        }

        table td {
            vertical-align: middle !important;
            text-align: center;
        }

        table th,
        table td {
            padding-top: 08px;
            padding-bottom: 08px;
        }

        .table-bordered {
            box-shadow: 0px 0px 5px 0.5px gray;
        }

        .table-bordered td,
        .table-bordered th {
            border: 1px solid #dee2e6;
        }

        .text-right {
            text-align: end;
        }

        .w-20 {
            width: 20%;
        }

        .float-right {
            float: right;
        }
    </style>
</head>

<body>

    <div class="container">

        <div class="body-section">
            <div class="row">
                <div class="col-6">
                    <h2 class="heading"><?= $order_transactions['merchant_nama'] ?></h2>
                    <p class="sub-heading">Nama : <?= $order_transactions['nama'] ?> </p>
                    <?php if ($order_transactions['metode'] == 'emoney') : ?>
                        <p class="sub-heading">Saldo : <?= "Rp " . number_format($order_transactions['saldo_awal'], 0, ',', '.')  ?> </p>
                        <p class="sub-heading">Sisa Saldo : <?= "Rp " . number_format($order_transactions['saldo_akhir'], 0, ',', '.')   ?></p>
                    <?php endif ?>
                    <p class="sub-heading">Metode Pembayaran : <?= $order_transactions['metode']   ?></p>
                    <p class="sub-heading">Tanggal : <?= $order_transactions['created_at']   ?></p>
                </div>
            </div>
        </div>

        <div class="body-section">
            <h3 class="heading">
                <?php
                $carts = $items;
                $jumlah = 0;
                foreach ($carts as $cart) {
                    $jumlah += $cart['jumlah'];
                }
                echo $jumlah;
                ?> Barang
            </h3>
            <br>
            <table class="table-bordered">
                <thead>
                    <tr>
                        <th>Produk</th>
                        <th class="w-20">Harga</th>
                        <th class="w-20">Jumlah</th>
                        <th class="w-20">Total</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($items as $item) : ?>
                        <tr>
                            <td><?= $item['nama'] ?></td>
                            <td><?= "Rp " . number_format($item['harga_jual'], 0, ',', '.')  ?></td>
                            <td><?= $item['jumlah'] ?></td>
                            <td><?= "Rp " . number_format($item['harga_jual'] * $item['jumlah'], 0, ',', '.')  ?></td>
                        </tr>
                    <?php endforeach ?>
                    <tr>
                        <td colspan="3" class="text-right">Total Harga</td>
                        <td><?= "Rp " . number_format($order_transactions['harga'], 0, ',', '.')  ?></td>
                    </tr>
                </tbody>
            </table>
            <br>

        </div>


    </div>

</body>

</html>