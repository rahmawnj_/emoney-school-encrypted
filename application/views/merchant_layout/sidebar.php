<aside id="sidebar" class="sidebar js-sidebar">
	<div class="sidebar-content js-simplebar">
		<a class="sidebar-brand" href="index.html">
			<span class="sidebar-brand-text align-middle">
				<?= $this->session->userdata('name') ?>
			</span>
			<svg class="sidebar-brand-icon align-middle" width="32px" height="32px" viewbox="0 0 24 24" fill="none" stroke="#FFFFFF" stroke-width="1.5" stroke-linecap="square" stroke-linejoin="miter" color="#FFFFFF" style="margin-left: -3px">
				<path d="M12 4L20 8.00004L12 12L4 8.00004L12 4Z"></path>
				<path d="M20 12L12 16L4 12"></path>
				<path d="M20 16L12 20L4 16"></path>
			</svg>
		</a>

		<div class="sidebar-user">
			<div class="d-flex justify-content-center">
				<div class="flex-shrink-0">
					<img src="<?= base_url('/assets/img/uploads/' . $this->session->userdata('gambar') ?? '') ?>" class="avatar img-fluid rounded me-1" alt="<?= $this->session->userdata('username') ?>">
				</div>
				<div class="flex-grow-1 ps-2">
					<div class="sidebar-user-subtitle"><?= $this->session->userdata('username') ?></div>
					<div class="sidebar-user-subtitle"><?= $this->session->userdata('role') ?></div>
				</div>
			</div>
		</div>

		<ul class="sidebar-nav">

			<?php if ($this->session->userdata('role') == 'owner'  || $this->session->userdata('role') == 'Viewer') : ?>
				<li class="sidebar-item <?= ($title == 'Dashboard') ? 'active' : ''; ?>">
					<a class="sidebar-link " href="<?= base_url('merchant/dashboard') ?>">
						<i class="align-middle" data-feather="sliders"></i> <span class="align-middle">Dashboard</span>
					</a>
				</li>
			<?php endif ?>


			<li class="sidebar-item <?= ($title == 'Katalog') ? 'active' : ''; ?>">
				<a class="sidebar-link " href="<?= base_url('merchant/catalogs') ?>">
					<i class="align-middle" data-feather="shopping-cart"></i> <span class="align-middle">Belanja</span>
				</a>
			</li>
			<?php if ($this->session->userdata('role') == 'owner'  || $this->session->userdata('role') == 'Viewer') : ?>
				<li class="sidebar-item <?= ($title == 'Produk') ? 'active' : ''; ?>">
					<a class="sidebar-link " href="<?= base_url('merchant/products') ?>">
						<i class="align-middle" data-feather="shopping-bag"></i> <span class="align-middle">Produk</span>
					</a>
				</li>
			<?php endif ?>

			<?php if ($this->session->userdata('role') == 'owner'  || $this->session->userdata('role') == 'Viewer') : ?>
				<li class="sidebar-item <?= ($title == 'Kategori') ? 'active' : ''; ?>">
					<a class="sidebar-link " href="<?= base_url('merchant/categories') ?>">
						<i class="align-middle" data-feather="grid"></i> <span class="align-middle">Kategori</span>
					</a>
				</li>
			<?php endif ?>

			<li class="sidebar-item <?= ($title == 'Order Transaksi') ? 'active' : ''; ?>">
				<a class="sidebar-link " href="<?= base_url('merchant/order_transactions') ?>">
					<i class="align-middle" data-feather="dollar-sign"></i> <span class="align-middle">Order Transaksi</span>
				</a>
			</li>

			<li class="sidebar-item <?= ($title == 'Cek Saldo') ? 'active' : ''; ?>">
				<a class="sidebar-link " href="<?= base_url('merchant/check_balance') ?>">
					<i class="align-middle" data-feather="credit-card"></i> <span class="align-middle">Cek Saldo</span>
				</a>
			</li>



		</ul>

	</div>
</aside>