-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3307
-- Waktu pembuatan: 16 Mar 2023 pada 05.25
-- Versi server: 10.4.25-MariaDB
-- Versi PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `emoney_school`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `carts`
--

CREATE TABLE `carts` (
  `id_cart` int(11) NOT NULL,
  `id_merchant` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `jumlah_harga` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `categories`
--

CREATE TABLE `categories` (
  `id_category` int(11) NOT NULL,
  `kategori` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `categories`
--

INSERT INTO `categories` (`id_category`, `kategori`, `created_at`, `updated_at`) VALUES
(1, 'Makanan', '2023-03-16 02:59:32', NULL),
(2, 'Minum', '2023-03-16 02:59:41', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `classes`
--

CREATE TABLE `classes` (
  `id_class` int(11) NOT NULL,
  `kelas` varchar(50) NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `classes`
--

INSERT INTO `classes` (`id_class`, `kelas`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'vii', 0, '2023-03-16 02:44:15', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `devices`
--

CREATE TABLE `devices` (
  `id_device` int(11) NOT NULL,
  `device` varchar(100) NOT NULL,
  `harga` int(11) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `lokasi` varchar(100) DEFAULT NULL,
  `created_at` text NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `devices`
--

INSERT INTO `devices` (`id_device`, `device`, `harga`, `gambar`, `lokasi`, `created_at`, `updated_at`, `deleted`) VALUES
(1, '1', 1000, 'devices/qrcode_localhost.png', 'area a', '2023-03-16 09:52:01', NULL, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `merchants`
--

CREATE TABLE `merchants` (
  `id_merchant` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `merchants`
--

INSERT INTO `merchants` (`id_merchant`, `nama`, `gambar`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'sosstore', 'merchants/sisisbakar.jpg', 0, '2023-03-16 03:00:25', '2023-03-16 03:00:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `merchant_accounts`
--

CREATE TABLE `merchant_accounts` (
  `id_merchant_account` int(11) NOT NULL,
  `id_merchant` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(20) NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `merchant_accounts`
--

INSERT INTO `merchant_accounts` (`id_merchant_account`, `id_merchant`, `username`, `password`, `role`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 'sosstoreowner', '$2y$10$1Iive765H3zEByZ7hZrD5ubpfuy4w.n4lgTeaIOYtwFcq9xCYtE5m', 'owner', 0, '2023-03-16 03:00:29', '2023-03-16 03:00:29'),
(2, 1, 'sosstorekasir', '$2y$10$2h2TSju4jwMNwaJj.2Dxj.M25KKSYIt1dfTKycv/oRI//cuFp0KGW', 'cashier', 0, '2023-03-16 03:00:29', '2023-03-16 03:00:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `order_transactions`
--

CREATE TABLE `order_transactions` (
  `id_order_transaction` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_merchant` int(11) NOT NULL,
  `saldo_awal` varchar(50) NOT NULL,
  `saldo_akhir` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL,
  `profit` int(11) NOT NULL,
  `status` varchar(10) DEFAULT NULL,
  `metode` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `order_transactions`
--

INSERT INTO `order_transactions` (`id_order_transaction`, `nama`, `id_merchant`, `saldo_awal`, `saldo_akhir`, `harga`, `profit`, `status`, `metode`, `created_at`) VALUES
(1, 'dims', 1, '-', '-', 20000, 10000, 'sukses', 'cash', '2023-03-16 03:10:50'),
(2, 'mi', 1, '9000', '9000', 40000, 0, 'gagal', 'emoney', '2023-03-16 03:11:52'),
(3, 'mi', 1, '59000', '19000', 40000, 20000, 'sukses', 'emoney', '2023-03-16 03:12:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id_product` int(11) NOT NULL,
  `id_merchant` int(11) NOT NULL,
  `id_category` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `stok` int(11) NOT NULL,
  `harga_modal` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id_product`, `id_merchant`, `id_category`, `nama`, `gambar`, `stok`, `harga_modal`, `harga_jual`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'ayam', 'products/Fried_Chicken-1024x536.png', 462, 10000, 20000, 0, '2023-03-16 03:10:12', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `purchased_items`
--

CREATE TABLE `purchased_items` (
  `id_purchased_item` int(11) NOT NULL,
  `id_order_transaction` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `jumlah` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `purchased_items`
--

INSERT INTO `purchased_items` (`id_purchased_item`, `id_order_transaction`, `id_product`, `harga`, `jumlah`) VALUES
(1, 1, 1, 20000, 1),
(2, 3, 1, 20000, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id_role` int(11) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id_role`, `role`) VALUES
(1, 'administrator'),
(2, 'admin_topup'),
(3, 'admin_absensi'),
(4, 'Viewer'),
(5, 'operator_absensi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `settings`
--

CREATE TABLE `settings` (
  `id_setting` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `settings`
--

INSERT INTO `settings` (`id_setting`, `name`, `value`) VALUES
(1, 'secret_key', 'tzk-ssi2023');

-- --------------------------------------------------------

--
-- Struktur dari tabel `students`
--

CREATE TABLE `students` (
  `id_student` int(11) NOT NULL,
  `id_class` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `nis` varchar(50) NOT NULL,
  `saldo` varchar(255) NOT NULL,
  `rfid` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(10) NOT NULL,
  `pin` varchar(100) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `no_hp_ortu` varchar(20) DEFAULT NULL,
  `foto` varchar(255) NOT NULL,
  `deleted` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `students`
--

INSERT INTO `students` (`id_student`, `id_class`, `nama`, `nis`, `saldo`, `rfid`, `jenis_kelamin`, `pin`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `no_hp`, `no_hp_ortu`, `foto`, `deleted`, `updated_at`, `created_at`) VALUES
(1, 1, 'mi', '56767', 'uu29RF0=', '5756756', 'Laki-laki', '$2y$10$KqIDzRft6xDxvlwV8Tah9uiAYoOKz2hUzT99eHrStJ8rWkT.BCiJu', '75657', '2023-03-20', '765', '756756', NULL, 'students/ice-cream-png.jpg', 0, '2023-03-16 03:12:33', '2023-03-16 02:45:06');

-- --------------------------------------------------------

--
-- Struktur dari tabel `student_parking_transactions`
--

CREATE TABLE `student_parking_transactions` (
  `id_student_parking_transaction` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `id_device` int(11) NOT NULL,
  `saldo_awal` int(11) NOT NULL,
  `saldo_akhir` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `status` varchar(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `student_parking_transactions`
--

INSERT INTO `student_parking_transactions` (`id_student_parking_transaction`, `id_student`, `id_device`, `saldo_awal`, `saldo_akhir`, `harga`, `status`, `created_at`) VALUES
(1, 1, 1, 0, 0, 1000, 'gagal', '2023-03-16 02:53:34'),
(2, 1, 1, 10000, 9000, 1000, 'sukses', '2023-03-16 02:54:23');

-- --------------------------------------------------------

--
-- Struktur dari tabel `teacher_staffs`
--

CREATE TABLE `teacher_staffs` (
  `id_teacher_staff` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `foto` varchar(128) NOT NULL,
  `rfid` varchar(128) NOT NULL,
  `jabatan` varchar(128) NOT NULL,
  `deleted` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL,
  `no_hp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `teacher_staffs`
--

INSERT INTO `teacher_staffs` (`id_teacher_staff`, `nama`, `foto`, `rfid`, `jabatan`, `deleted`, `created_at`, `updated_at`, `no_hp`) VALUES
(1, '354354', 'teacher_staffs/Fried_Chicken-1024x536.png', '5353453', 'Guru', 0, '2023-03-16 02:43:53', NULL, '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `teacher_staff_parking_transactions`
--

CREATE TABLE `teacher_staff_parking_transactions` (
  `id_teacher_staff_parking_transaction` int(11) NOT NULL,
  `id_teacher_staff` int(11) NOT NULL,
  `id_device` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `teacher_staff_parking_transactions`
--

INSERT INTO `teacher_staff_parking_transactions` (`id_teacher_staff_parking_transaction`, `id_teacher_staff`, `id_device`, `created_at`) VALUES
(1, 1, 1, '2023-03-16 02:52:29'),
(2, 1, 1, '2023-03-16 02:52:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `top_ups`
--

CREATE TABLE `top_ups` (
  `id_top_up` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nominal` int(11) NOT NULL,
  `saldo_awal` int(11) NOT NULL,
  `saldo_akhir` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `top_ups`
--

INSERT INTO `top_ups` (`id_top_up`, `id_student`, `id_user`, `nominal`, `saldo_awal`, `saldo_akhir`, `created_at`) VALUES
(1, 1, 10, 10000, 0, 10000, '2023-03-16 02:54:17'),
(2, 1, 1, 50000, 9000, 59000, '2023-03-16 03:12:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `id_role` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id_user`, `id_role`, `nama`, `foto`, `username`, `password`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 1, 'Administrator', 'users/oga76v6k.png', 'admin', '$2y$10$h2uwSZch946wMiBjYjICVeZLxaHZTPllPiHB6f0ApnBsAiuSWAT1C', NULL, '2022-08-24 13:28:22', 0),
(2, 2, 'Admin Topup', 'users/9081cbefe7c689caed05a251d1ed78721.png', 'topup', '$2y$10$QQpsvEe8NjVEq/6c6dNZhu/j91igbtkP3Q9u60EbPR8GUiaDu3JeC', '2022-07-31 12:01:18', NULL, 1),
(3, 1, 'Admin', 'users/emoney.png', 'administrator', '$2a$12$uYMT8ZfTP25x6b6ZuwjyoeWCUHJYYCfmKPy5zV4JZH9TrItUWWHxW', NULL, '2022-08-31 06:35:05', 1),
(4, 3, 'Admin Absensi', 'users/new2.jpeg', 'absensi', '$2a$12$yS4RLWB.qfNiwJnowDa06.NZB6ntXy6woRXIsMJqG5sXw8lbxPh8S', NULL, '2022-11-08 07:19:13', 1),
(5, 3, 'ds9038', 'users/', '543859843', '$2y$10$N18Y3KlS/upvr6FYsvbdp.ZZrICuK2J2AaTqNVMbtdgd2LfvasHlS', '2022-11-08 06:10:29', NULL, 1),
(6, 1, 'rewrew543543', 'users/WhatsApp_Image_2022-11-02_at_11_55_25_(1).jpeg', 'admin543435', '$2y$10$dgF7ia0ITNvQnjqInc3ZW.6zFHgoDrXfx9M76OEVp.ss77KAxP8oK', '2022-11-08 06:11:34', '2022-11-08 06:15:22', 1),
(7, 2, '87898', 'users/IMG-20221103-WA0030.jpg', 'adminjhkjhkiu', '$2y$10$e942MvSc.BrOYfSEmegiJeuEdNXfcrGlIzWRlE6FOFjSsIGT5ajnu', '2022-11-08 07:00:13', NULL, 1),
(8, 4, 'Viewer', '', 'viewer', '$2y$10$g8DJQ25WGzXH.v2P6ZwcEuUcbLkafR46FIJIftDgpbqOehENDDQ4W', '2023-02-15 07:23:01', NULL, 0),
(9, 5, 'miko', 'users/WhatsApp_Image_2023-03-02_at_14_21_11.jpeg', 'miko', '$2y$10$a4eDOCm7xMzFuA3k7IzNPOphaTMQDzgBvlOds2CP6FYCNKzpO/jqW', '2023-03-03 07:46:37', NULL, 1),
(10, 2, 'topup', 'users/The-school-visit-what-to-look-for-what-to-ask-1.jpg', 'admtopup', '$2y$10$afHTXL.S0yo4BV/WKGCUSuLjS3R.e5F86jcYkaCHooMvHRf7kD3L.', '2023-03-16 02:41:30', '2023-03-16 02:43:00', 0);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id_cart`);

--
-- Indeks untuk tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id_category`);

--
-- Indeks untuk tabel `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id_class`);

--
-- Indeks untuk tabel `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id_device`);

--
-- Indeks untuk tabel `merchants`
--
ALTER TABLE `merchants`
  ADD PRIMARY KEY (`id_merchant`);

--
-- Indeks untuk tabel `merchant_accounts`
--
ALTER TABLE `merchant_accounts`
  ADD PRIMARY KEY (`id_merchant_account`);

--
-- Indeks untuk tabel `order_transactions`
--
ALTER TABLE `order_transactions`
  ADD PRIMARY KEY (`id_order_transaction`);

--
-- Indeks untuk tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id_product`);

--
-- Indeks untuk tabel `purchased_items`
--
ALTER TABLE `purchased_items`
  ADD PRIMARY KEY (`id_purchased_item`);

--
-- Indeks untuk tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id_role`);

--
-- Indeks untuk tabel `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indeks untuk tabel `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id_student`);

--
-- Indeks untuk tabel `student_parking_transactions`
--
ALTER TABLE `student_parking_transactions`
  ADD PRIMARY KEY (`id_student_parking_transaction`);

--
-- Indeks untuk tabel `teacher_staffs`
--
ALTER TABLE `teacher_staffs`
  ADD PRIMARY KEY (`id_teacher_staff`);

--
-- Indeks untuk tabel `teacher_staff_parking_transactions`
--
ALTER TABLE `teacher_staff_parking_transactions`
  ADD PRIMARY KEY (`id_teacher_staff_parking_transaction`);

--
-- Indeks untuk tabel `top_ups`
--
ALTER TABLE `top_ups`
  ADD PRIMARY KEY (`id_top_up`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `carts`
--
ALTER TABLE `carts`
  MODIFY `id_cart` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `id_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `classes`
--
ALTER TABLE `classes`
  MODIFY `id_class` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `devices`
--
ALTER TABLE `devices`
  MODIFY `id_device` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `merchants`
--
ALTER TABLE `merchants`
  MODIFY `id_merchant` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `merchant_accounts`
--
ALTER TABLE `merchant_accounts`
  MODIFY `id_merchant_account` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `order_transactions`
--
ALTER TABLE `order_transactions`
  MODIFY `id_order_transaction` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `products`
--
ALTER TABLE `products`
  MODIFY `id_product` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `purchased_items`
--
ALTER TABLE `purchased_items`
  MODIFY `id_purchased_item` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `settings`
--
ALTER TABLE `settings`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `students`
--
ALTER TABLE `students`
  MODIFY `id_student` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `student_parking_transactions`
--
ALTER TABLE `student_parking_transactions`
  MODIFY `id_student_parking_transaction` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `teacher_staffs`
--
ALTER TABLE `teacher_staffs`
  MODIFY `id_teacher_staff` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `teacher_staff_parking_transactions`
--
ALTER TABLE `teacher_staff_parking_transactions`
  MODIFY `id_teacher_staff_parking_transaction` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `top_ups`
--
ALTER TABLE `top_ups`
  MODIFY `id_top_up` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
